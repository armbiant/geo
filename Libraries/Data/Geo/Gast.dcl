definition module Data.Geo.Gast

from Data.Geo import :: Position, :: CellId
from Gast     import generic ggen, generic genShow, :: GenState

derive genShow Position, CellId

/**
 * This generates positions such that the distance between most consecutive positions,
 * is a distance which objects (such as cars, ships, ...) can realistically move in 1 second.
 */
derive ggen Position

derive ggen CellId
