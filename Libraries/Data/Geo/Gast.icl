implementation module Data.Geo.Gast

import StdEnv, StdOverloadedList
import Data.Geo, Data.Functor, Data.List
import Control.GenBimap
import Gast

genShow{|Position|} sep p position rest = genShow{|*|} sep p (latitudeOf position, longitudeOf position) rest

ggen{|Position|} _ =
	[! position lat lng
	\\ (lat, lng)
	<- diag2
		[0.0,  -89.0,  90.0: (\x -> toReal x / 10000.0) <$> [-10..10]]
		[0.0, -179.0, 180.0: (\x -> toReal x / 10000.0) <$> [-10..10]]
	]

derive genShow CellId

ggen{|CellId|} st = (\(c1, c2, c3, c4, c5, c6, c7, c8) -> CellId {c1, c2, c3, c4, c5, c6, c7, c8}) <$> ggen{|*|} st