definition module Data.Geo._S2

from System._Finalized import :: Finalizer
from Data.GenDefault import generic gDefault

// default is angles in degrees, distances in meters
import System._Pointer

:: S2RegionPtr =: S2RegionPtr Finalizer

//* Gives `?None` if the loop is invalid.
s2Loop :: !{#Real} !{#Real} !Int -> ?S2RegionPtr

//* Gives `?None` if the cap is invalid.
s2Cap :: !S2LatLng !Real -> ?S2RegionPtr

//* Gives `?None` if the rect is invalid.
s2LatLngRect :: !Real !Real !Real !Real -> ?S2RegionPtr

//* Pointer to an S2 line.
:: S2LinePtr =: S2LinePtr Finalizer

/**
 * Returns a pointer to an S2 line.
 *
 * @param The lat and lng for first point of the line.
 * @param The lat and lng for second point of the line.
 * @result The line connecting the points.
 */
s2Line :: !S2LatLng !S2LatLng -> S2LinePtr

// this is a C++ object living on the Clean heap
:: S2LatLng =: S2LatLng {#Char}

s2LatLng :: !Real !Real -> S2LatLng
s2LatLngGetLat :: !S2LatLng -> Real
s2LatLngGetLng :: !S2LatLng -> Real

distanceBetweenLatLng :: !S2LatLng !S2LatLng -> Real

latLngIsInside :: !S2LatLng !S2RegionPtr -> Bool
cellIdOfLatLng :: !S2LatLng -> {#Char}
coveringCellIdRanges :: !S2RegionPtr -> [(String, String)]

sizeOfS2LatLng :: Int

areaOfS2Loop :: !S2RegionPtr -> Real
areaOfS2Cap :: !S2RegionPtr -> Real
areaOfS2LatLngRect :: !S2RegionPtr -> Real

/**
 * Returns whether two s2 lines intersect.
 *
 * @param The first line.
 * @param The second line.
 * @result Whether the first and second line intersect.
 */
s2LinesIntersect :: !S2LinePtr !S2LinePtr -> Bool

derive gDefault S2LatLng
