#### 1.1.2

- Fix: bug in `rectRegionToBoundaryLines` causing erroneous lines to be returned by the function for the west/south boundaries.

#### 1.1.1

- Chore: switch to `abseil-cpp` branch `lts_2022_06_23`.

### 1.1.0

- Feature: add function to create lines and check if the lines intersect with other lines or if the lines intersect with the boundary lines of a `:: RectRegion`.

#### 1.0.2

- Chore: upgrade base-compiler-itasks version constraint from =1.0.0 to ^1.0.0 || ^2.0.0

#### 1.0.1

- Robustness: add triangle inequality property to `Data.Geo.distanceBetween`

## 1.0.0

- Initial version